<!DOCTYPE html>
<html>
<head></head>

<body>
    <nav>
        <ul>
            <li><a href="views/about.view.php">About</a></li>
            <li><a href="views/contact.view.php">Contact</a></li>
        </ul>
    </nav>
    <h1>My tasks</h1>
    <ul>
        <?php foreach ($tasks as $task) : ?>
            <li>
            <?php if ($task -> completed) : ?>
                <strike><?= $task->description; ?></strike>
            <?php else : ?>
                <?=$task->description; ?>
            <?php endif ?>
            </li>
        <?php endforeach; ?>
    <ul>
</body>
</html>  