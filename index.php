<?php

$database = require 'core/bootstrap.php';
require 'controllers/index.php';

$router = new Router;
require 'routes.php';
require $router->direct('about'); 